from os import system

def bilangan_prima (bilangan):
    for i in range(2, bilangan):
        if bilangan % i == 0:
            return False

    return True

def list_bilangan_prima (b_awal, b_akhir):
    list_bilangan_prima = []

    for i in range(b_awal, b_akhir + 1):
        if bilangan_prima(i):
            list_bilangan_prima.append(i)

    return list_bilangan_prima

system('cls')
str_jenis_input = '''Program pencarian bilangan prima

Pilih jenis pencarian :
1. Cek apakah bilangan termasuk bilangan prima atau tidak
2. Mencari bilangan prima dengan range angka

Masukkan input = '''

jenis_input = input(str_jenis_input)

if jenis_input == '1':
    system('cls')
    for x in range(3):
        bilangan = input('Masukkan bilangan = ')
        try:
            bilangan = int(bilangan)
            break
        except ValueError:
            print('input bilangan harus berupa angka')
            system('cls')
    
    if x >= 2:
        print('teralu banyak percobaan input harap ulangi beberapa saat lagi')
        exit()
    else:
        prima = bilangan_prima(bilangan)
        if prima == True:
            print(f'bilangan {bilangan} termasuk bilangan prima')
        else:
            print(f'bilangan {bilangan} tidak termasuk bilangan prima')
    
elif jenis_input == '2':
    system('cls')
    for x in range(3):
        b_awal = input('Masukkan bilangan awal = ')
        b_akhir = input('Masukkan bilangan akhir = ')
        try:
            b_awal = int(b_awal)
            b_akhir = int(b_akhir)
            break
        except ValueError:
            print('input bilangan harus berupa angka')
            system('cls')
    
    if x >= 2:
        print('teralu banyak percobaan input harap ulangi beberapa saat lagi')
        exit()
    else:
        prima = list_bilangan_prima(b_awal, b_akhir)
        
        print(f'berikut list bilangan prima mulai dari {b_awal} sampai {b_akhir}\n{prima}')
else:
    print('input tidak tersedia')